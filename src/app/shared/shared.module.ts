import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokeHeaderComponent } from './poke-header/poke-header.component';
import { PokeSearchComponent } from './poke-search/poke-search.component';
import { PokeListComponent } from './poke-list/poke-list.component';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PokemonComponent } from './pokemon/pokemon.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { DetailsComponent } from '../pages/details/details.component';


@NgModule({
  declarations: [
    PokeHeaderComponent,
    PokeSearchComponent,
    PokeListComponent,
    PokemonComponent,
    DetailsComponent
  ],
  exports: [
    PokeHeaderComponent,
    PokeSearchComponent,
    PokeListComponent,
    PokemonComponent,
    DetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule, IonicModule,
    Ng2SearchPipeModule,FormsModule
  ]
})
export class SharedModule {
}



