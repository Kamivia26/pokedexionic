import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { PokeApiService } from 'src/app/service/poke-api.service';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'poke-list',
  templateUrl: './poke-list.component.html',
  styleUrls: ['./poke-list.component.scss'],
})
export class PokeListComponent implements OnInit {

  limit = 20;
  offset = 0;
  public getAllPokemons: any;
  public setAllPokemons: any;
  public apiError = false;
  term: string;

    // eslint-disable-next-line @typescript-eslint/member-ordering
    @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  pokemons: any[] =[];

  totalPokemons!: number;

  constructor(
    private pokeApiService: PokeApiService
    ) { }

  ngOnInit(): void {
    this.getPokemon(this.limit, 0);
  }

  getPokemon(limit: number, offset: number) {
    this.pokeApiService
      .getPokemon(this.limit, this.offset)
      .subscribe((res: any) => {
        this.pokemons = [...this.pokemons, ...res.results];
        this.getAllPokemons = this.pokemons;
        console.log(res);
      } ) ;
  }
  loadPokemon(event) {
    setTimeout(() => {
      console.log('Done');

      this.offset += 10;
      this.getPokemon(this.limit, this.offset);
      event.target.complete();
      console.log('1', this.pokemons.length);
    } ,  1000 ) ;
  }

  public getSearch(value: string){
    const filter = this.pokemons.filter( (res: any) =>!res.name.indexOf(value.toLowerCase()));
    this.getAllPokemons = filter;
  }

  onSearchChange(event) {
    const value = event.detail.value;
    if (value === '') {
      this.offset = 0;
      this.loadPokemon(event);
    }
    this.pokeApiService.findPokemon(value).subscribe((res) => {
      this.pokemons = [res];
      console.log('res:', res);
      console.log('pokemons:', this.pokemons);
    });
  }
}

