import { Component, Input, OnInit } from '@angular/core';
import { PokeApiService } from 'src/app/service/poke-api.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent implements OnInit {
  @Input() pokemon!: any;
  pokemonDetail: any;
  constructor(private pokeApiService: PokeApiService) {}
  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.pokeApiService.getData(this.pokemon.name).subscribe((res) => {
      this.pokemonDetail = res;
      console.log(res);
    } ) ;
  }

}
