/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokeApiService {
  private url = environment.serverAPI;
  private imgUrl = environment.imgUrl;
  private urlDetails = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=1126';

  constructor(
    private http: HttpClient
  ) { }

  getMethod(link: string,param?: any): Observable<any>{
    const content = JSON.stringify(param);
    const option: any = {
       body: content,
    };
    return this.http.request('get',this.url+link,option);
  }


  getPokemon(limit: number, offset: number) {
    return this.http.get(this.url + `pokemon?limit=${limit}&offset=${offset}`);
  }
  getData(name: string) {
    return this.http.get(this.url + `pokemon/${name}`);
  }

  getPokeImage(index){
    return `${this.imgUrl}${index}.png`;
  }
  findPokemon(search) {
    return this.http.get(`${this.url}pokemon/${search}`).pipe(
      map((pokemon) => {
        // eslint-disable-next-line @typescript-eslint/dot-notation
        pokemon['image'] = this.getPokeImage(pokemon['id']);
        // eslint-disable-next-line @typescript-eslint/dot-notation
        pokemon['pokeIndex'] = pokemon['id'];
        return pokemon;
      })
    );
  }

  public apiGetPokemons( urlDetails: string): Observable<any>{
    return this.http.get<any>(urlDetails).pipe(
      map(
        res => res
      )
    );
  }
}
