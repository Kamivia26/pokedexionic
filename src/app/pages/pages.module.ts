import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Module Routing
import { RoutingModule } from './routing.module';
//Module

//Pages
import { HomeComponent } from './home/home.component';
import { IonicModule } from '@ionic/angular';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SharedModule } from '../shared/shared.module';
import { NavigationComponent } from './navigation/navigation.component';
import { TabsPageModule } from '../tabs/tabs.module';



@NgModule({
  declarations: [
    HomeComponent,
    NavigationComponent,
  ],
  imports: [
    CommonModule,
    RoutingModule,
    SharedModule,IonicModule,TabsPageModule
  ]
})
export class PagesModule { }
